#$language = "python"
#$interface = "1.0"

import subprocess
import os
from subprocess import PIPE,Popen

def init_D_command(process, parameter):
	command = "/etc/init.d/" + str(process)
	change_status_1 = subprocess.call([command, parameter])
	#change_status_1.stdout.close()
	return
		
def get_status(process):
	
	command = "/etc/init.d/" + str(process)
	process_status_1 = subprocess.Popen([command, "status"], stdout=PIPE)
	process_status_2 = subprocess.Popen(["grep", "Active"], stdin=process_status_1.stdout, stdout=PIPE)
	status = process_status_2.stdout.read()
	process_status_1.stdout.close()
	process_status_2.stdout.close()

	return str(status)

def set_status(process):
	command = "/etc/init.d/" + str(process)
	status_str = get_status(process)
	if "inactive" in status_str:
		init_D_command(process, "start")
	else:
		init_D_command(process, "stop")
	
	return
	
	
		
set_status("ntp")