#$language = "python"
#$interface = "1.0"

import subprocess
from bs4 import BeautifulSoup
from subprocess import PIPE,Popen

def get_status(process):
	
	command = "/etc/init.d/" + str(process)
	process_status_1 = subprocess.Popen([command, "status"], stdout=PIPE)
	process_status_2 = subprocess.Popen(["grep", "Active"], stdin=process_status_1.stdout, stdout=PIPE)
	status = process_status_2.stdout.read()
	process_status_1.stdout.close()
	process_status_2.stdout.close()

	return str(status)
	
def get_version(package):
	version_command_1 = subprocess.Popen(["dpkg", "-l"], stdout=PIPE)
	version_command_2 = subprocess.Popen(["grep", "^ii"], stdin=version_command_1.stdout, stdout=subprocess.PIPE)
	version_tmp = version_command_2.stdout.read()
	version_command_1.stdout.close()
	version_command_2.stdout.close()
	
	version_tmp = version_tmp.split("\n")
	for version_line in version_tmp:
		if str(package) in version_line:
			version = version_line
	
	return str(version)
	
def edit_web_page(ntp_version, ntp_status, bs_version, apache_version):

	with open("/var/www/html/index.html") as inf:
		txt = inf.read()
		soup = BeautifulSoup(txt, "lxml")


	ntp_version_div = soup.new_tag('div')
	ntp_status_div = soup.new_tag('div')
	bs_version_div = soup.new_tag('div')
	apache_version_div = soup.new_tag('div')
	
	ntp_version_div.string = "NTP version: " + ntp_version
	ntp_status_div.string = "NTP status: " + ntp_status
	bs_version_div.string = "Beautiful Soup version: " + bs_version
	apache_version_div.string = "Apache version: " + apache_version
	
	soup.body.append(ntp_status_div)
	soup.body.append(ntp_version_div)
	soup.body.append(bs_version_div)
	soup.body.append(apache_version_div)
	soup.body.append(soup.new_tag('br'))	
		

	with open("/var/www/html/index.html", "w") as outf:
		outf.write(str(soup))
	return
		
		
edit_web_page(get_version("ntp"), get_status("ntp"), get_version("python-bs4"), get_version("apache2"))